create schema scheduler;

create table scheduler.loop(id integer primary key, value text);

create publication publoop for table only scheduler.loop with (publish = 'update');

do $$
  begin
    execute format(
      'create subscription subloop connection %s publication publoop with (create_slot = false, enabled = false)',
       quote_literal('port='||inet_server_port()||' user='||current_user||' dbname='||current_database())
    );
  end;
$$ language plpgsql;

select pg_create_logical_replication_slot('subloop', 'pgoutput');

alter subscription subloop enable;

create table test(id integer);

create or replace function scheduler.event() returns trigger as $$
begin
  perform pg_sleep(1);
  insert into test(id) values((random() * 1000)::integer);
  return new;
exception when others then
  raise exception '%', sqlerrm;
end;
$$ language plpgsql;

create trigger loop_event after update on scheduler.loop for each row execute procedure scheduler.event();

alter table scheduler.loop enable replica trigger loop_event;
